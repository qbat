<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name></name>
    <message>
        <location filename="constants.h" line="5"/>
        <source>QBat - Qt Battery Monitor</source>
        <translation>QBat - Qt Batterie Monitor</translation>
    </message>
</context>
<context>
    <name>qbat::CBatteryIcon</name>
    <message>
        <location filename="batteryicon.cpp" line="68"/>
        <source>QBat - %1: %2%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="77"/>
        <location filename="batteryicon.cpp" line="87"/>
        <location filename="batteryicon.cpp" line="97"/>
        <location filename="batteryicon.cpp" line="100"/>
        <source>status: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="77"/>
        <source>discharging</source>
        <translation>entladen</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="83"/>
        <location filename="batteryicon.cpp" line="93"/>
        <source>remaining time: %1:%2</source>
        <translation>Verbleibende Zeit: %1:%2</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="87"/>
        <source>charging</source>
        <translation>laden</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="97"/>
        <source>full</source>
        <translation>voll</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="100"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="110"/>
        <location filename="batteryicon.cpp" line="130"/>
        <source>current rate: %1W / %2A</source>
        <translation>Aktuelle Rate: %1W / %2A</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="113"/>
        <source>current rate: %1W</source>
        <translation>Aktuelle Rate: %1W</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="116"/>
        <source>current capacity: %1mWh</source>
        <translation>aktuelle Kapazität: %1mWh</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="119"/>
        <source>last full capacity: %1mWh</source>
        <translation>volle Kapazität: %1mWh</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="122"/>
        <source>design capacity: %1mWh</source>
        <translation>Designkapazität: %1mWh</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="133"/>
        <source>current rate: %1A</source>
        <oldsource>current rate: %2A</oldsource>
        <translation>Aktuelle Rate: %1A</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="136"/>
        <source>current capacity: %1mAh</source>
        <translation>aktuelle Kapazität: %1mAh</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="139"/>
        <source>last full capacity: %1mAh</source>
        <translation>volle Kapazität: %1mAh</translation>
    </message>
    <message>
        <location filename="batteryicon.cpp" line="142"/>
        <source>design capacity: %1mAh</source>
        <translation>Designkapazität: %1mAh</translation>
    </message>
</context>
<context>
    <name>qbat::CPowerManager</name>
    <message>
        <location filename="powermanager.cpp" line="28"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="29"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="31"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="33"/>
        <source>merged</source>
        <translation>zusammengefasst</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="170"/>
        <source>AC adapter plugged in</source>
        <translation>AC Adapter angeschlossen</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="172"/>
        <source>AC adapter unplugged</source>
        <translation>AC Adapter getrennt</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="186"/>
        <source>no information available</source>
        <translation>Keine Informationen verfügbar</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="264"/>
        <source>QBat - critical battery capacity (will automatically choose ok on timeout)</source>
        <translation>QBat - Kritischer Ladestand (automatische Wahl von OK bei Ablauf der Zeit)</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="265"/>
        <source>QBat - critical battery capacity</source>
        <translation>QBat - Kritischer Ladestand</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="267"/>
        <source>WARNING: The attached battery(s) reached the critical mark.
Click cancel and please make sure to save and shut down soon or provide another source of power
or:
Click ok to execute:
%1</source>
        <translation>WARNUNG: Die angeschlossenen Batterien haben den Kritischen Ladestand erreicht.
Durch den Klick auf OK wird folgender Befehl Ausgeführt:
%1

Wenn Sie auf Abbrechen klicken sollten Sie den PC an eine Stromquelle anschließen oder
ihre Arbeit möglichst bald speichern und den PC herunterfahren.</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="271"/>
        <source>WARNING: The attached battery(s) reached the critical mark.
Please make sure to save and shut down soon or provide another source of power.</source>
        <translation>WARNUNG: Die angeschlossenen Batterien haben den Kritischen Ladestand erreicht.
Sie sollten den PC an eine Stromquelle anschließen oder ihre Arbeit möglichst bald speichern und den PC herunterfahren.</translation>
    </message>
    <message>
        <location filename="powermanager.cpp" line="319"/>
        <location filename="powermanager.cpp" line="322"/>
        <location filename="powermanager.cpp" line="330"/>
        <source>About QBat</source>
        <translation>Über QBat</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="settingsdialog.ui" line="14"/>
        <source>QBat - Settings</source>
        <translation>QBat - Einstellungen</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="20"/>
        <source>Event for critical battery capacity</source>
        <translation>Ereignis für kritischen Ladestand</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="29"/>
        <source>Critical capacity (in %)</source>
        <translation>Kritischer Ladestand (in %)</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="36"/>
        <source>Show warning message</source>
        <translation>Zeige nur eine Warnung</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="46"/>
        <source>Execute command</source>
        <translation>Führe einen Befehl aus</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="69"/>
        <source>Confirm command</source>
        <translation>Bestätige Befehl</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="78"/>
        <source>Use timeout to confirm</source>
        <translation>Zeitbegrenzte Bestätigung</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="98"/>
        <source>Seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="140"/>
        <source>Battery icon(s)</source>
        <translation>Batteriesymbole</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="159"/>
        <source>Main color</source>
        <translation>Grundfarbe</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="166"/>
        <location filename="settingsdialog.ui" line="169"/>
        <location filename="settingsdialog.ui" line="203"/>
        <location filename="settingsdialog.ui" line="206"/>
        <location filename="settingsdialog.ui" line="230"/>
        <location filename="settingsdialog.ui" line="233"/>
        <source>full</source>
        <translation>voll</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="176"/>
        <location filename="settingsdialog.ui" line="179"/>
        <source>charged</source>
        <translation>Ladestand</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="186"/>
        <location filename="settingsdialog.ui" line="189"/>
        <source>empty</source>
        <translation>leer</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="196"/>
        <source>Text color</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="213"/>
        <location filename="settingsdialog.ui" line="216"/>
        <location filename="settingsdialog.ui" line="240"/>
        <location filename="settingsdialog.ui" line="243"/>
        <source>(dis)charging</source>
        <translation>(ent)ladend</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="223"/>
        <source>&quot;+ pole&quot; color</source>
        <translation>+Polfarbe</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="250"/>
        <source>Merge Icons</source>
        <translation>Fasse Symbole zusammen</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="260"/>
        <source>Show messages as balloon tips</source>
        <translation>Zeige Nachrichten als Sprechblasen</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="275"/>
        <source>Polling rate (in milliseconds)</source>
        <translation>Aktualisierungsrate (in Millisekunden)</translation>
    </message>
</context>
</TS>
